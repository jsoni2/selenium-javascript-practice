


describe('Protractor Jasmine Example', function() {

    // Open the www.kayak.com website in the browser before each test is run
    beforeEach(function() {
        browser.waitForAngularEnabled(false);  
        browser.get('https://www.kayak.com/');
    });

    // // Test to ensure we are on the home page by checking the <body> tag id attribute
    // it('Should be on the home page', function() {
    //     const bElement = element(by.tagName('body'));
    //     bElement.getAttribute('id').then(function(id) {
    //         expect(id).toBeTruthy();
    //     });
    // });

    // it('should test title of kayak', () => {
    //     expect(browser.getTitle()).toEqual('Search Flights, Hotels & Rental Cars | KAYAK');
    // });

    it('should click on origin airport', () => {
        // browser.driver.sleep(30000);
        const originElement = element(by.xpath('//*[@class="_ej _KE _ht _bDp _F4"]'));
        // const destinationElement = element(by.xpath('//div[@class="_GH _kQ _b8 _b7 _btL _v _dR _qN _bB- _bBg _bAc _h3 _lZ _bCa _bCb _bBa _H7 _bB7 _bB8 _bB9 _hA _z3 _bB5 _bB6 _bA5 _bCl _np _bCm _bCn _bCo _wZ _Ag _bCh _bCi _l1 _bCj _bCk _cee _Jv _cp _rD _eGY _bCp _r0 _fJS _bCq _bCr _bCs selectTextOnFocus _Qw"]'))
        const destinationElement = element(by.xpath('//div[@class="_GH _kQ _b8 _b7 _btL _v _dR _qN _bB- _bBg _bAc _h3 _lZ _bCa _bCb _bBa _H7 _bB7 _bB8 _bB9 _hA _z3 _bB5 _bB6 _bA5 _bCl _np _bCm _bCn _bCo _wZ _Ag _bCh _bCi _l1 _bCj _bCk _cee _Jv _cp _rD _eGY _bCp _r0 _fJS _bCq _bCr _bCs selectTextOnFocus _Qw"]'));
        const destinationInput = element(by.xpath('//input[@class="_bEn _bEj _bEk _bEl _bEm _bEf _bEg _bEh _bEi _bEb _bEc _bEd _bEe _bEa size-m _D _bDF _bDG _vA _K _bxU _bxV _bxR _bxS _r _bxT _kA _VL _VK _kC _VM _kB _w _kD _y _zz _q _SV _kQ _fK _bDV _qM _n"]'));
        // originElement.click();
        // browser.pause();
        // expect(element(by.xpath('//*[@class="_bEn _bEj _bEk _bEl _bEm _bEf _bEg _bEh _bEi _bEb _bEc _bEd _bEe _bEa size-m _D _bDF _bDG _vA _K _bxU _bxV _bxR _bxS _r _bxT _kA _VL _VK _kC _VM _kB _w _kD _y _zz _q _SV _kQ _fK _bDV _qM _n smarty-complete"]')).getText()).toBe('some');
        // expect(browser.driver.findElement(by.xpath('//*[@id="NOii-origin-airport"]')).getAttribute('id')).toBeTruthy();
        const departureDate = element(by.xpath('//div[@class="col-start js-startDateCol _dx _bwF _bwG _cg _q _4 _k _n"]'));
        const arrivalDate = element(by.xpath('//div[@class="col-end js-endDateCol _cg _dx _q _4 _k _bwF _bwG _n"]'));
        // originElement.click();
        // originElement.sendKeys('San Jose');
        // originElement.sendKeys(protractor.Key.ESCAPE);
        // await destinationElement.click();
        // destinationElement.click();
        browser.driver.sleep(3000);
        destinationElement.sendKeys('Chicago');
        browser.driver.sleep(3000);
        destinationInput.sendKeys(protractor.Key.ESCAPE);
        browser.driver.sleep(30000);
    });

});