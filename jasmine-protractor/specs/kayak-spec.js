const KayakAppPage = require('../pages/kayakPage');
const KayakAppSearchResultPage = require('../pages/kayakSearchResultPage');
const KayakAppData = require('../data/kayakAppData.json');
const using = require('jasmine-data-provider');
const EC = protractor.ExpectedConditions;

describe('Search Flight on Kayak', () => {

    const kayakHomePage = new KayakAppPage('https://www.kayak.com/');

    beforeAll(() => {
        kayakHomePage.get();
    });

    it('submit button should work', () => {
        kayakHomePage.submitButton.click();
        browser.wait(EC.visibilityOf(kayakHomePage.anywhereTitle), 60000);
        expect(kayakHomePage.anywhereTitle.getText()).toBe('You didn\'t select an airport');
        browser.actions().sendKeys(protractor.Key.ESCAPE).perform();
    });

    it('should check text in heading', () => {
        expect(kayakHomePage.headingOne.getText()).toBe('Search hundreds of travel sites at once.');
    });

    it('should check text in origin', () => {
        expect(kayakHomePage.origin.getText()).toBe('San Francisco (SFO)');
    });

    it('should check text in destination', () => {
        expect(kayakHomePage.destination.getText()).toBe('To?');
    });

    it('should check start date text', () => {
        expect(kayakHomePage.startDate.getText()).toBe('Depart');
    });

    it('should check return date text', () => {
        expect(kayakHomePage.returnDate.getText()).toBe('Return');
    });

    it('should check default flight type', () => {
        expect(kayakHomePage.flightTypeLabel.getText()).toBe('Round-trip');
    });

    it('should check default traveller number', () => {
        expect(kayakHomePage.travellersLabel.getText()).toBe('1 Adult');
    });

    it('should check default cabin type', () => {
        expect(kayakHomePage.cabinLabel.getText()).toBe('Economy');
    });

    using(KayakAppData.FlightSearch, (data) => {
        
        it('Select destination and Select Dates to search flight', () => {
            kayakHomePage.setDestinationInput(data.destination);
            kayakHomePage.selectStartAndReturnDates(data.startDate, data.returnDate);
            kayakHomePage.searchForFlight();
        });
    })

    

});

// describe('Search Result Page should Display', () => {

//     const kayakSearchResultPage = new KayakAppSearchResultPage('https://www.kayak.com/flights/SFO-CHI/2019-05-24/2019-05-24');

//     beforeAll(() => {
//         kayakSearchResultPage.get();
//     });

//     it('Should Display Search Result List', () => {
//         kayakSearchResultPage.checkResultList();
//     });
// });