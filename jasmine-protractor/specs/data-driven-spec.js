const AppDemoPage = require('../pages/appDemoPage');
const AppDemoData = require('../data/appDemoData.json');

describe('Data Driven Approach Demo', () => {
    const appHomePage = new AppDemoPage('http://juliemr.github.io/protractor-demo/');

    beforeAll(() => {
        appHomePage.get();
    });

    beforeEach(() => {
        appHomePage.firstNumber.clear();
        appHomePage.secondNumber.clear();
    });

    AppDemoData.calcData.forEach((data) => {
        it(`${data.operator.toLowerCase()} of ${data.input1} and ${data.input2} should be ${data.expectedResult}`, () => {
            appHomePage.calc(data.input1, data.input2, data.operator);
            expect(appHomePage.result.getText()).toEqual(data.expectedResult.toString());
        })
    })
});