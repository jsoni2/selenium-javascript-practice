const AppDemoPage = require('../pages/appDemoPage');
const AppDemoData = require('../data/appDemoData.json');
const using = require('jasmine-data-provider');

describe('Data Driven Approach Utilizing Using from Jasmine Data Provider', () => {
    const appHomePage = new AppDemoPage('http://juliemr.github.io/protractor-demo/');

    beforeAll(() => {
        appHomePage.get();
    });

    beforeEach(() => {
        appHomePage.firstNumber.clear();
        appHomePage.secondNumber.clear();
    });

    using(AppDemoData.calcData, (data) => {
        it(`${data.operator.toLowerCase()} of ${data.input1} and ${data.input2} should be ${data.expectedResult}`, () => {
            appHomePage.calc(data.input1, data.input2, data.operator);
            expect(appHomePage.result.getText()).toEqual(data.expectedResult.toString());
        })
    })
});