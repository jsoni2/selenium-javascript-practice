const AppDemoPage = require('../pages/appDemoPage');
const AppDemoData = require('../data/appDemoDataObject.json');
const using = require('jasmine-data-provider');

describe('Data Driven Approach Utilizing Using from Jasmine Data Provider', () => {
    const appHomePage = new AppDemoPage('http://juliemr.github.io/protractor-demo/');

    beforeAll(() => {
        appHomePage.get();
    });

    beforeEach(() => {
        appHomePage.firstNumber.clear();
        appHomePage.secondNumber.clear();
    });

    using(AppDemoData, (data, description) => {
        it(`${description}`, () => {
            appHomePage.calc(data.input1, data.input2, data.operator);
            expect(appHomePage.result.getText()).toEqual(data.expectedResult.toString());
        })
    })
});