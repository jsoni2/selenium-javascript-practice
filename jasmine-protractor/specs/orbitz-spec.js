const OrbitzAppPage = require('../pages/orbitz/orbitzHomePage');
const OrbitzAppResultPage = require('../pages/orbitz/orbitzSearchResultPage');
const AppData = require('../data/orbitzAppData.json');
const using = require('jasmine-data-provider');

describe('Search Flight on Orbitz', () => {

    const orbitzHomePage = new OrbitzAppPage('https://www.orbitz.com/Flights');
    // const orbitzAppResultPage = new OrbitzAppResultPage();

    beforeAll(() => {
        orbitzHomePage.get();
    });

    it('Check Title of Orbitz Flight Page', () => {
        const orbitzLaunchPageTitle = orbitzHomePage.launchPageTitle.getText();
        expect(orbitzLaunchPageTitle).toBe('Search Flights');
    });

    using(AppData.FlightSearch, (data) => {
        const orbitzAppResultPage = new OrbitzAppResultPage();
        it('Should enter Origin City', () => {
            orbitzHomePage.setOrigin(data.origin);
        });
    
        it('Should enter Destination City', () => {
            orbitzHomePage.setDestination(data.destination);
        });

        it('Should enter Departing and Return Date', () => {
            orbitzHomePage.setDepartingAndReturnDate(data.startDate, data.returningDate);
        });


        it('should click search', () => {
            orbitzHomePage.searchFlight();
            orbitzAppResultPage.checkResultPage();
        });

        it('should check result list', () => {
            orbitzAppResultPage.checkResultList();
        });

        
    });

    

});