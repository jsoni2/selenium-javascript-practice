const AppDemoPage = require('../pages/appDemoPage');

describe('some scenarios in step1', () => {
    const appHomePage = new AppDemoPage('http://juliemr.github.io/protractor-demo/');

    beforeEach(() => {
        appHomePage.get(); 
    });

    
// testDat.forEach(data => {
//     it('check the history for multiply operations', () => {
//         appHomePage.calc(4,3, 'MULTIPLICATION');
//         appHomePage.calc(5,3, 'MULTIPLICATION');
//         appHomePage.calc(7,3, 'MULTIPLICATION');
//         expect(appHomePage.history.count()).toEqual(3);
//     });
// });
    // tc4
    it('check the history for multiply operations', () => {
        appHomePage.calc(4,3, 'MULTIPLICATION');
        appHomePage.calc(5,3, 'MULTIPLICATION');
        appHomePage.calc(7,3, 'MULTIPLICATION');
        expect(appHomePage.history.count()).toEqual(3);
    });

    // tc5
    it('check the history for division operations', () => {
        appHomePage.calc(4,3, 'DIVISION');
        appHomePage.calc(5,3, 'DIVISION');
        appHomePage.calc(7,3, 'DIVISION');
        expect(appHomePage.history.count()).toEqual(3);
    });

});