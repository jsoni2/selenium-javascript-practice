const HomePage = require('../pages/homePage');
const AppDemoPage = require('../pages/appDemoPage');

describe('Enter name feature', () => {
    it('should enter name tom', () => {
        const homePage = new HomePage('https://angularjs.org/');
        homePage.get();
        homePage.setName('Tom');
        const text = element(by.xpath('/html/body/div[2]/div[1]/div[2]/div[2]/div/h1'));
        expect(text.getText()).toBe('Hello Tom!');
    });
});

describe('multiple scenarios', () => {
    const appHomePage = new AppDemoPage('http://juliemr.github.io/protractor-demo/');

    beforeEach(() => {
        appHomePage.get(); 
    });

    // tc1
    it('should have a title', () => {
        expect(browser.getTitle()).toEqual('Super Calculator');
    });

    // tc2
    it('check the history for add operations', () => {
        appHomePage.calc(4,3, 'ADDITION');
        appHomePage.calc(5,3, 'ADDITION');
        appHomePage.calc(7,3, 'ADDITION');
        expect(appHomePage.history.count()).toEqual(3);
    });

    // tc3
    it('check the history for subtract operations', () => {
        appHomePage.calc(4,3, 'SUBTRACTION');
        appHomePage.calc(5,3, 'SUBTRACTION');
        appHomePage.calc(7,3, 'SUBTRACTION');
        expect(appHomePage.history.count()).toEqual(3);
    });

    // // tc4
    // it('check the history for multiply operations', () => {
    //     appHomePage.calc(4,3, 'MULTIPLICATION');
    //     appHomePage.calc(5,3, 'MULTIPLICATION');
    //     appHomePage.calc(7,3, 'MULTIPLICATION');
    //     expect(appHomePage.history.count()).toEqual(3);
    // });

    // // tc5
    // it('check the history for division operations', () => {
    //     appHomePage.calc(4,3, 'DIVISION');
    //     appHomePage.calc(5,3, 'DIVISION');
    //     appHomePage.calc(7,3, 'DIVISION');
    //     expect(appHomePage.history.count()).toEqual(3);
    // });

    // // tc6
    // it('check the history for modulo operations', () => {
    //     appHomePage.calc(4,3, 'MODULO');
    //     appHomePage.calc(5,3, 'MODULO');
    //     appHomePage.calc(7,3, 'MODULO');
    //     expect(appHomePage.history.count()).toEqual(3);
    // });
});