const EC = protractor.ExpectedConditions;
class OrbitzHomePage {
    constructor(url) {
        this.url = url;
        this.launchPageTitle = element(by.xpath('//h1[contains(@class,"wizard-tabs launch-page-title")]'));
        this.originInput = element(by.xpath('//input[@id="flight-origin-flp"]'));
        this.destinationInput = element(by.xpath('//input[@id="flight-destination-flp"]'));
        this.departingSection = element(by.xpath('//div[@id="flight-departing-wrapper-flp"]'));
        this.departingDateInput = element(by.xpath('//input[@id="flight-departing-flp"]'));
        this.returningDateInput = element(by.xpath('//input[@id="flight-returning-flp"]'));
        this.calendarNextButton = element(by.xpath('//button[@class="datepicker-paging datepicker-next btn-paging btn-secondary next"]'));
        this.searchButton = element(by.xpath('//form[@id="gcw-flights-form-flp"]//button[contains(@class,"btn-primary btn-action gcw-submit")]'))
    }

    get() {
        browser.waitForAngularEnabled(false);
        browser.get(this.url);
    }

    setOrigin(origin) {
        this.originInput.sendKeys(origin);
        this.originInput.sendKeys(protractor.Key.TAB);
        browser.driver.sleep(3000);
    }

    setDestination(destination) {
        this.destinationInput.sendKeys(destination);
        browser.driver.sleep(3000);
    }

    monthDiff(todaysDate, futureDate) {
        return (futureDate.getFullYear()*12 + futureDate.getMonth()) - (todaysDate.getFullYear()*12 + todaysDate.getMonth());
    }

    setDepartingDate(departingDate) {
        this.departingDateInput.sendKeys(departingDate);
        
        browser.driver.sleep(3000);
    }

    setReturningDate(departDate, returnDate) {
        const month = returnDate.getUTCMonth() + 1; //months from 1-12
        const day = returnDate.getUTCDate();
        const year = returnDate.getUTCFullYear();
        const returnDateElement = element(by.xpath(`//button[@data-year=${year} and @data-month=${month} and @data-day=${day}]`));
        this.returningDateInput.click();
        returnDate.setHours(0,0,0,0);
        let jumps = this.monthDiff(departDate, returnDate);
        do {
            this.calendarNextButton.click();
            jumps--;
        }
        while (jumps >= 0);
        browser.wait(EC.visibilityOf(returnDateElement), 60000);
        returnDateElement.click();
        
        browser.driver.sleep(3000);
    }
    setDepartingAndReturnDate(departingDate, returningDate) {
        this.setDepartingDate(departingDate);
        let departDate = new Date(departingDate);
        let returnDate = new Date(returningDate);
        let now = new Date();
        now.setHours(0,0,0,0);

        this.setReturningDate(departDate, returnDate);


    }

    searchFlight() {
        this.searchButton.click();
    }
}

module.exports = OrbitzHomePage;