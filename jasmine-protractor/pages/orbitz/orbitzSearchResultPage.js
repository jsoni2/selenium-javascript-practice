const EC = protractor.ExpectedConditions;

class OrbitzSearchResultPage {
    constructor() {
        browser.waitForAngularEnabled(false);
        this.titleCityText = element(by.xpath('//span[@class="title-city-text"]'));
        // this.destination = destination;
        this.listResult = element(by.xpath('//*[@id="flightModuleList"]'));
    }

    checkResultPage() {
        browser.wait(EC.visibilityOf(this.titleCityText), 60000);
        expect(this.titleCityText.getText()).toContain("Chicago");
    }

    checkResultList() {
        browser.wait(EC.visibilityOf(this.listResult), 60000);
        let items = this.listResult.all(by.xpath('//li//div[@class="uitk-col all-col-shrink"]//div[contains(@class,"primary-content")]//span[contains(@class, "full-bold")]'));
        
        let prices = [];
        items.each(function(ele, index) {
            ele.getText().then(function(text) {
                const price = text.match(/[+-]?\d+(\.\d+)?/g).map(function(v) {
                    prices.push(parseFloat(v));
                    return parseFloat(v);
                });
            })
        }).then(function(){
            const isSorted = prices.every((val, i, arr) => !i || (val >= arr[i - 1]));
            expect(isSorted).toBeTruthy();
        });
    }

}

module.exports = OrbitzSearchResultPage;
