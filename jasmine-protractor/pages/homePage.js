class HomePage {
    constructor(url) {
        this.url = url;
        this.nameInput = element(by.model('yourName'));
    }
    get() {
        browser.get(this.url);
    }
    setName(name) {
        this.nameInput.sendKeys(name);
    }
}

module.exports = HomePage;