
class AppDemoPage {
    constructor(url) {
        this.url = url;
        this.firstNumber = element(by.model('first'));
        this.secondNumber = element(by.model('second'));
        this.operator = element(by.model('operator'));
        this.goButton = element(by.id('gobutton'));
        this.result = element(by.xpath('//h2[@class="ng-binding"]'));
        this.history = element.all(by.repeater('result in memory'));
    }

    get() {
        browser.get(this.url);
    }

    setFirstNumber(number) {
        this.firstNumber.sendKeys(number);
    }

    setSecondNumber(number) {
        this.secondNumber.sendKeys(number);
    }

    calc(number1, number2, operator) {
        this.setFirstNumber(number1);
        this.setSecondNumber(number2);
        this.operator.element(by.css("option[value='"+operator+"']")).click();
        this.goButton.click();
    }
}

module.exports = AppDemoPage;