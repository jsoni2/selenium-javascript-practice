const EC = protractor.ExpectedConditions;

class KayakHomePage {
    constructor(url) {
        this.url = url;
        this.headingOne = element(by.xpath('//h2[@class="title dark"]'));
        this.origin = element(by.xpath('//*[@class="SearchPage__FrontDoor"]//div[contains(@id, "-origin-input-wrapper")]'));
        this.destination = element(by.xpath('//*[@class="SearchPage__FrontDoor"]//div[contains(@id, "-destination-input-wrapper")]'));
        this.destinationInput = element(by.xpath('//div[contains(@id,"-destination")]//*[@class="_bEn _bEj _bEk _bEl _bEm _bEf _bEg _bEh _bEi _bEb _bEc _bEd _bEe _bEa size-m _D _bDF _bDG _vA _K _bxU _bxV _bxR _bxS _r _bxT _kA _VL _VK _kC _VM _kB _w _kD _y _zz _q _SV _kQ _fK _bDV _qM _n"]'));
        this.startDate = element(by.xpath('//*[@class="SearchPage__FrontDoor"]//div[contains(@id,"-dates-col")]//div[contains(@id, "-dateRangeInput-display-start-inner")]'));
        this.returnDate = element(by.xpath('//*[@class="SearchPage__FrontDoor"]//div[contains(@id,"-dates-col")]//div[contains(@id, "-dateRangeInput-display-end-inner")]'));
        this.calendarPrevMonth = element(by.xpath('//div[@id="stl-jam-cal-prevMonth"]'));
        this.calendarNextMonth = element(by.xpath('//div[@id="stl-jam-cal-nextMonth"]'));
        this.submitButton = element(by.xpath('//*[@class="SearchPage__FrontDoor"]//div[contains(@id, "-col-button-wrapper")]'));
        this.anywhereTitle = element(by.xpath('//*[contains(@id,"-anywhere-search-title")]'));
        this.flightTypeLabel = element(by.xpath('//div[contains(@id, "-primary")]//div[contains(@id,"-switch-display-status")]'));
        this.travellersLabel = element(by.xpath('//div[contains(@id, "-primary")]//button[contains(@id,"-travelersAbove")]'));
        this.cabinLabel = element(by.xpath('//div[contains(@id, "-primary")]//div[contains(@class,"col-cabin")]'));
    }

    get() {
        browser.waitForAngularEnabled(false);
        browser.get(this.url);
    }

    monthDiff(todaysDate, futureDate) {
        return (futureDate.getFullYear()*12 + futureDate.getMonth()) - (todaysDate.getFullYear()*12 + todaysDate.getMonth());
    }

    selectStartDate(startDate) {
        startDate.setHours(0,0,0,0);
        const startDateElement = element(by.xpath(`//div[@data-val=${startDate.getTime()}]`));
        if (this.monthDiff(new Date(), startDate) < 2) {
            // Start Date can be selected from available UI
            browser.wait(EC.visibilityOf(startDateElement), 60000);
            startDateElement.click();
            
        } else {
            // To select the start date you must click next month until you get your desired month
            let jumps = this.monthDiff(new Date(), startDate);
            while (jumps > 1) {
                this.calendarNextMonth.click();
                jumps--;
            }
            browser.wait(EC.visibilityOf(startDateElement), 60000);
            startDateElement.click();
        }
    }

    selectReturnDate(startDate, returnDate) {
        const returnDateElement = element(by.xpath(`//div[@data-val=${returnDate.getTime()}]`));
        returnDate.setHours(0,0,0,0);
        let jumps = this.monthDiff(startDate, returnDate);
        do {
            this.calendarNextMonth.click();
            jumps--;
          }
        while (jumps >= 0);
        browser.wait(EC.visibilityOf(returnDateElement), 60000);
        returnDateElement.click();
    }

    selectStartAndReturnDates(startDate, returnDate) {
        browser.wait(EC.visibilityOf(this.startDate), 60000);
        this.startDate.click();
        let toBeStartDate = new Date(startDate);
        let toBeReturnDate = new Date(returnDate);
        let now = new Date();
        now.setHours(0,0,0,0);

        if (toBeStartDate < now || toBeReturnDate < now) {
            throw new Error('Please don\'t select Dates from past');
        } 

        this.selectStartDate(toBeStartDate);
        this.selectReturnDate(toBeStartDate, toBeReturnDate);
    }

    setDestinationInput(destination) {
        this.destination.click();
        browser.wait(EC.visibilityOf(this.destinationInput), 60000);
        this.destinationInput.sendKeys(destination);
        browser.actions().sendKeys(protractor.Key.ENTER).perform();
    }

    searchForFlight() {
        this.submitButton.click();
        // browser.driver.sleep(30000);
    }

}

module.exports = KayakHomePage;