const EC = protractor.ExpectedConditions;
class KayakSearchResultPage {
    constructor(url) {
        this.url = url;
        this.resultList = element(by.xpath('//div[@id="searchResultsList"]'));
    }

    get() {
        browser.waitForAngularEnabled(false);
        browser.get(this.url);
    }

    checkResultList() {
        browser.wait(EC.visibilityOf(this.resultList), 60000);
        expect(this.resultList.isPresent()).toBe(false);
    }
}

module.exports = KayakSearchResultPage;