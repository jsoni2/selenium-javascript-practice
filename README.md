# selenium-javascript-practice

## Using Jasmine and Protractor
To Install Protractor Globally
```
$npm install -g protractor
```
Download and start the selenium server with
```
webdriver-manager update
webdriver-manager start
```

## To run the Kayak test
Go to jasmine-protractor directory.
```
$cd jasmine-protractor
```
Install dependencies with

```
$npm install
```
Run the test with

```
$protractor conf.js // Runs all the files with **spec.js 
```

## Using Jasmine and Selenium
## To run the Kayak test
Go to jasmine-selenium directory.
```
$cd jasmine-selenium
```
Install dependencies with

```
$npm install
```
Run the test with

```
$npm test // Runs all the files with **spec.js 
```

## Using Mocha
### To run the Google search and Kayak test
Go to mocha-selenium directory.
```
$cd mocha-selenium
```
Install dependencies with

```
$npm install
$npm install –g –save mocha
```
Run the test with

```
$npm run test
```

## Added Report Generation To debug falling tests with stack trace and screen shots
![Screen Shot 2019-05-14 at 4 14 49 PM](https://user-images.githubusercontent.com/18238847/57738271-a1ce4300-7663-11e9-9b2c-124cb40a6525.png)

## Added Dashboard report for other stakeholders
![Screen Shot 2019-05-14 at 4 11 05 PM](https://user-images.githubusercontent.com/18238847/57738101-f7562000-7662-11e9-97e9-87c4ea4687ae.png)
![Screen Shot 2019-05-14 at 4 13 03 PM](https://user-images.githubusercontent.com/18238847/57738166-32585380-7663-11e9-8348-90a10c40cf57.png)

## Added Support for reporting

### Before 
![Screen Shot 2019-05-13 at 3 23 35 PM](https://user-images.githubusercontent.com/18238847/57658719-c1e30100-7594-11e9-8571-8824920cecf8.png)

### After

#### Reporting all successful Test cases
![Screen Shot 2019-05-13 at 3 22 29 PM](https://user-images.githubusercontent.com/18238847/57658753-dc1cdf00-7594-11e9-94ab-43123d11ce6e.png)

#### Reporting successful and failed Test cases
![Screen Shot 2019-05-13 at 3 21 37 PM](https://user-images.githubusercontent.com/18238847/57658818-0f5f6e00-7595-11e9-8d61-39cc77621215.png)

