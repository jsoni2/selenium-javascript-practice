

var selenium = require('selenium-webdriver');

describe('Selenium Jasmine Example', function() {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 100000;

    // Open the www.kayak.com website in the browser before each test is run
    beforeEach(function(done) {
        this.driver = new selenium.Builder().
            withCapabilities(selenium.Capabilities.chrome()).
            build();

        this.driver.get('https://www.kayak.com/').then(done);
    });

    // Close the website after each test is run (so that it is opened fresh each time)
    afterEach(function(done) {
        this.driver.quit().then(done);
    });

    // Test to ensure we are on the home page by checking the <body> tag id attribute
    it('Should be on the home page', function(done) {
        
        var element = this.driver.findElement(selenium.By.tagName('body'));

        element.getAttribute('id').then(function(id) {
            expect(id).toBeTruthy();
            done();
        });
    });

    it('Should have title with expected text content', function(done) {
        
        var titleElement = this.driver.findElement(selenium.By.className('title-section'));

        titleElement.getText().then(function(content) {
            expect(content).toBe('Search hundreds of travel sites at once.');
            done();
        });
    });

});