// Import required packages

require('chromedriver');

const assert = require('assert');
const {Builder, Key, By, until} = require('selenium-webdriver');

describe('Testing Google.com', function () {
    let driver;
    before(async function() {
        driver = await new Builder().forBrowser('chrome').build();
    });
    // Get element selector from browser inspect feature.

    it('Search on Google', async function() {
        // Load the page
        await driver.get('https://google.com');
        // Find the search box by XPath
        await driver.findElement(By.xpath('//*[@id="tsf"]/div[2]/div/div[1]/div/div[1]/input')).click();
        // Enter keywords and click enter
        await driver.findElement(By.xpath('//*[@id="tsf"]/div[2]/div/div[1]/div/div[1]/input')).sendKeys('something', Key.RETURN);
        // Wait for the results box by id
        await driver.wait(until.elementLocated(By.id('rcnt')), 10000);
        // We will get the title value and test it
        let title = await driver.getTitle();
        assert.equal(title, 'something - Google Search');
    });
    // close the browser after running tests
    after(() => driver && driver.quit());
})

describe('Testing on Kayak', async () => {
    let driver;
    before(async () => {
        driver = await new Builder().forBrowser('chrome').build();
    });

    it('Search on Kayak', async () => {
        // Load the page
        await driver.get('https://www.kayak.com/');
        
        // We will get the title value and test it
        let title = await driver.getTitle();
        assert.equal(title, 'Search Flights, Hotels & Rental Cars | KAYAK');
    });
    // close the browser after running tests
    after(() => driver && driver.quit());
});